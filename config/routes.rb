Rails.application.routes.draw do

  root to: 'pages#index'

  mount TryApi::Engine => '/developers'

  match 'api/*all' => 'api/base#cors_preflight_check', :constraints => {:method => 'OPTIONS'}, :via => [:options]

  scope '(:locale)' do
    namespace :admin do
      resources :users,            only: [:index, :create, :update, :destroy, :show]
      resources :piechart_data,    only: :index
    end

    namespace :api do
      namespace :v1 do
        post :login, to: 'sessions#create'
        delete :logout, to: 'sessions#destroy'
        post :push_token, to: 'sessions#update'

        post :signup, to: 'users#create'
        get :profile, to: 'users#profile'
        # put :update_profile, to: 'users#update'
        # post :upgrade_role, to: 'users#change_user_role'

        resources :users,                only: [:index, :show]
        resources :contacts,             only: [:index, :create, :update, :destroy, :show]
      end
    end

    resources :sessions, only: [:create] do
      collection do
        delete :destroy
        get :check
      end
    end
  end
end
