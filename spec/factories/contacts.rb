FactoryBot.define do
  factory :contact do
    abc = 'abcdefg'
    user { FactoryBot.create(:user, :client) }

    sequence(:firstName)     { |n| "user#{abc[n]}" }
    sequence(:lastName)      { |n| "user#{abc[n]}" }
    sequence(:email)         { |n| "#{n}@test.com" }
    sequence(:phone)         { |n| "12345678#{n}" }
  end
end