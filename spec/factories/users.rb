FactoryBot.define do
  factory :user do
    abc = 'abcdefg'
    sequence(:firstName)     { |n| "user#{abc[n]}"  }
    sequence(:lastName)      { |n| "user#{abc[n]}"  }
    sequence(:email)   { |n| "#{n}@test.com"   }

    password                'secret!'
    password_confirmation   'secret!'
    # avatar {File.new("#{Rails.root}/spec/fixtures/factory_image.png")}

    trait :client do
      role { Role.get_client }
    end

    trait :admin do
      role { Role.get_admin }
    end
  end
end