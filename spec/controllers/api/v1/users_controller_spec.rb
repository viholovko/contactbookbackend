require 'rails_helper'
require 'benchmark'

RSpec.describe Api::V1::UsersController, type: :controller do
  render_views
  require 'rails_helper'

  describe '#create' do
    it 'should create user' do
      post :create, params: {
          email: 'test@test.com',
          lastName: 'user',
          firstName: 'user',
          password: 'secret'
      }

      response_body = JSON.parse(response.body)

      expect(response.status).to eq(200)
      expect(response_body['user']['firstName']).to eq('user')
      expect(response_body['user']['lastName']).to eq('user')
      expect(response_body['user']['email']).to eq('test@test.com')
    end

    it 'should render error if firstName not string' do
      post :create, params: {
        email: 'test@test.com',
        lastName: 'user',
        firstName: 'user1',
        password: 'secret'
      }

      response_body = JSON.parse(response.body)

      expect(response.status).to be(400)
      expect(response_body['errors'][0]['message']).to eq('First name must be letters only')
    end

    it 'should render error if firstName empty' do
      post :create, params: {
        email: 'test@test.com',
        lastName: 'user',
        password: 'secret'
      }

      response_body = JSON.parse(response.body)

      expect(response.status).to be(400)
      expect(response_body['errors'][0]['message']).to eq("First name can't be blank.")
    end

    it 'should render validation error for already registered email' do
      user = create :user, :client

      post :create, params: {
        email: user.email,
        lastName: user.firstName,
        firstName: user.lastName,
        password: user.password
      }

      response_body = JSON.parse(response.body)

      expect(response.status).to be(400)
    end
  end

  # describe '#update' do
  #   it 'should create user' do
  #     country = create :country
  #     user = create :user, :not_upgraded, country_id: country.id, phone_number: '123456789'
  #     sign_in user: user
  #
  #     put :update, params: {
  #         lastName: 'userupdated',
  #         firstName: 'userupdated',
  #         about_me: 'test updated'
  #     }
  #
  #     response_body = JSON.parse(response.body)
  #
  #     expect(response.status).to be(200)
  #     expect(response_body['user']['firstName']).to eq('userupdated')
  #     expect(response_body['user']['lastName']).to eq('userupdated')
  #     expect(response_body['user']['about_me']).to eq('test updated')
  #   end
  #
  #   it 'should render validation errors' do
  #     country = create :country
  #     user = create :user, :not_upgraded, country_id: country.id, phone_number: '123456789'
  #     sign_in user: user
  #
  #     put :update, params: {
  #         lastName: '',
  #         firstName: ''
  #     }
  #
  #     response_body = JSON.parse(response.body)
  #
  #     expect(response.status).to be(400)
  #     expect(response_body['errors'][0]['code']).to eq('E00103')
  #     expect(response_body['errors'][1]['code']).to eq('E00110')
  #     expect(response_body['errors'][2]['code']).to eq('E00104')
  #     expect(response_body['errors'][3]['code']).to eq('E00111')
  #   end
  # end

  describe '#show' do
    let(:user1) {create :user, :client}
    let(:user2) {create :user, :client}
    let(:response_body) {JSON.parse(response.body)}

    it 'should return error if user not found' do
      sign_in user: user1

      get :show, params: {id: -1}
      expect(response.status).to be 400

      expect(response_body['errors'][0]['message']).to eq('Record not found.')
    end

    it 'should return error if user not login' do

      get :show, params: {id: user1.id}
      expect(response.status).to be 400

      expect(response_body['errors'][0]['message']).to eq('Access denied.')
    end

  end
end
