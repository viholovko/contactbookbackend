require 'rails_helper'

RSpec.describe Api::V1::SessionsController, type: :controller do
  render_views
  describe '#create' do
    it 'should create session' do
      user = create :user, :client

      post :create, params: {email: user.email, password: user.password}

      response_body = JSON.parse(response.body)

      expect(response.status).to be(200)
      expect(response_body['session_token']).to_not be_nil
      expect(response_body['user']['id']).to eq(user.id)
    end

    it 'should not create session if there is no request code' do
      user = create :user, :client

      post :create, params: {email: user.email, password: "#{user.password}1111"}

      response_body = JSON.parse(response.body)

      expect(response.status).to be(400)
    end
  end

  describe '#destroy' do
    it 'should destroy session' do
      user = create :user, :client
      session = sign_in user: user

      delete :destroy, params: {session_token: session.token}

      response_body = JSON.parse(response.body)

      expect(response.status).to be(200)
      expect(user.sessions.count).to eq(0)
    end

    it 'should render unauthorized if there is no session' do
      delete :destroy, params: {session_token: ''}

      response_body = JSON.parse(response.body)

      expect(response.status).to be(400)
    end
  end
end
