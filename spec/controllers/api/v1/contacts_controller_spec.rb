require 'rails_helper'
require 'benchmark'

RSpec.describe Api::V1::ContactsController, type: :controller do
  render_views
  require 'rails_helper'

  describe '#create' do
    let(:user) {create :user, :client}
    let(:response_body) {JSON.parse(response.body)}

    it 'should create user contact' do
      sign_in user: user

      post :create, params: {
          email: 'test@test.com',
          lastName: 'user',
          firstName: 'user',
          phone: '12357899'
      }

      expect(response.status).to eq(200)
      expect(response_body['contact']['firstName']).to eq('user')
      expect(response_body['contact']['lastName']).to eq('user')
      expect(response_body['contact']['email']).to eq('test@test.com')
      expect(response_body['contact']['phone']).to eq('12357899')
    end

    it 'should render error if firstName not string' do
      sign_in user: user

      post :create, params: {
        email: 'test@test.com',
        lastName: 'user',
        firstName: 'user1',
        phone: '12357899'
      }

      response_body = JSON.parse(response.body)

      expect(response.status).to be(400)
      expect(response_body['errors'][0]['message']).to eq('First name must be letters only')
    end

    it 'should render error if firstName empty' do
      sign_in user: user

      post :create, params: {
        email: 'test@test.com',
        lastName: 'user1',
        phone: '12357899'
      }

      response_body = JSON.parse(response.body)

      expect(response.status).to be(400)
      expect(response_body['errors'][0]['message']).to eq("First name can't be blank.")
    end

    it 'should render error if lastName not string' do
      sign_in user: user

      post :create, params: {
        email: 'test@test.com',
        lastName: 'user1',
        firstName: 'user',
        phone: '12357899'
      }

      response_body = JSON.parse(response.body)

      expect(response.status).to be(400)
      expect(response_body['errors'][0]['message']).to eq('Last name must be letters only')
    end

    it 'should render error if lastName empty' do
      sign_in user: user

      post :create, params: {
        email: 'test@test.com',
        firstName: 'user',
        phone: '12357899'
      }

      response_body = JSON.parse(response.body)

      expect(response.status).to be(400)
      expect(response_body['errors'][0]['message']).to eq("Last name can't be blank.")
    end

    it 'should render error if phone number not numeric' do
      sign_in user: user

      post :create, params: {
        email: 'test@test.com',
        lastName: 'user',
        firstName: 'user',
        phone: '12357899a'
      }

      response_body = JSON.parse(response.body)

      expect(response.status).to be(400)
      expect(response_body['errors'][0]['message']).to eq('Phone number must be numeric')
    end

  end

  describe '#destroy' do
    let(:user) {create :user, :client}
    let(:user1) {create :user, :client}
    let(:contact) {create :contact}

    it 'should destroy contact' do
      sign_in user: user

      post :create, params: {
        email: 'test@test.com',
        lastName: 'user',
        firstName: 'user',
        phone: '12357899'
      }

      _contact = JSON.parse(response.body)
      expect(Contact.count).to be 1


      delete :destroy, params: {id: _contact['contact']['id']}

      delete_responce = JSON.parse(response.body)

      expect(response.status).to be 200
      expect(delete_responce['message']).to eq('Contact is deleted')
      expect(Contact.count).to be 0
    end

    it 'should return error for destroy contact which not exist' do
      sign_in user: user

      delete :destroy, params: {id: -1}

      delete_responce = JSON.parse(response.body)

      expect(response.status).to be 400
      expect(delete_responce['errors'][0]['message']).to eq('Record not found.')
    end
  end

  describe '#update' do
    let(:user) { create :user, :client }
    let(:response_body) { JSON.parse(response.body) }

    it 'should return update contact' do
      sign_in user: user

      post :create, params: {
        email: 'test@test.com',
        lastName: 'user',
        firstName: 'user',
        phone: '12357899'
      }

      put :update, params: { id: response_body['contact']['id'], email: 'test1@test.com', lastName: 'userA', firstName: 'userA',  phone: '12357897' }
      expect(response.status).to be 200
    end
  end

  describe '#index' do
    let(:user) { create :user, :client }
    let(:response_body) { JSON.parse(response.body) }

    it 'should return contacts' do
      sign_in user: user

      post :create, params: {
        email: 'test@test.com',
        lastName: 'user',
        firstName: 'user',
        phone: '12357899'
      }

      get :index

      index_responce = JSON.parse(response.body)

      expect(response.status).to be 200
      expect(index_responce['count']).to be 1
      expect(index_responce['contacts'][0]['firstName']).to eq('user')
      expect(index_responce['contacts'][0]['lastName']).to eq('user')
      expect(index_responce['contacts'][0]['email']).to eq('test@test.com')
      expect(index_responce['contacts'][0]['phone']).to eq('12357899')

    end
  end

  describe '#show' do
    let(:user) {create :user, :client}
    let(:user1) {create :user, :client}
    let(:contact) {create :contact}
    let(:response_body) {JSON.parse(response.body)}

    it 'should return contact' do
      sign_in user: user

      post :create, params: {
        email: 'test@test.com',
        lastName: 'user',
        firstName: 'user',
        phone: '12357899'
      }

      get :show, params: {id: response_body['contact']['id']}

      _contact = JSON.parse(response.body)

      expect(response.status).to be 200

      expect(_contact['contact']['firstName']).to eq(response_body['contact']['firstName'])
      expect(_contact['contact']['lastName']).to eq(response_body['contact']['lastName'])
      expect(_contact['contact']['email']).to eq(response_body['contact']['email'])
      expect(_contact['contact']['phone']).to eq(response_body['contact']['phone'])
    end

    it 'should return error if user not found' do
      sign_in user: user

      get :show, params: {id: -1}

      expect(response.status).to be 400

      expect(response_body['errors'][0]['message']).to eq('Record not found.')
    end

    it 'should return error if user not authorized' do
      get :show, params: {id: contact.id}

      expect(response.status).to be 400

      expect(response_body['errors'][0]['message']).to eq('Access denied.')
    end

    it 'should return error if contact not current user' do
      sign_in user: user

      post :create, params: {
        email: 'test@test.com',
        lastName: 'user',
        firstName: 'user',
        phone: '12357899'
      }

      sign_out

      sign_in user: user1

      get :show, params: {id: response_body['contact']['id']}

      _contact = JSON.parse(response.body)

      expect(response.status).to be 403
    end

  end
end
