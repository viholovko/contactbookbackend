  The main idea of this project to show knowledge and experience on Ruby and Ruby on Rails. All users have the 
possibility to create and manage personal protected contact, in this project. Additionally to this system added 
different user's roles. In the project used CanCanCan which restricts what resources a given user is allowed to access.
    
The project consists of three parts:
  - Rest API,
  - Small admin CRM with verification,
  - Try_Api - documentation and request/response test equipment (use URL with '/developers' to check this functionality)
    
* TDD: RSpec. 

* Database
   - PostgreSQL: 11.7.2
  
* System dependencies
   - Ruby version: 2.4.0,
   - Ruby On Rails version: 5.1.6,
   - ReactJS version: ^15.4.1 
   - NPM version: 5.3.0

* Configuration
    - configure config/database.yml (Set database name and credential for access.) 

* Database creation
    - rails db:create  
    - rails db:migrate
     
* Database initialization
    - rails db:seed (Add 'admin' user to system.)
    
* How to run the test suite
   - rspec spec
   
* How to start project
    - set all credentials;
    - install all ruby dependency;
    - install npm dpendency (npm install);
    - generate assets for project;
    - start server.
