class User < ApplicationRecord

  has_many :sessions, dependent: :destroy
  has_many :contacts, dependent: :destroy
  belongs_to :role
  attr_accessor :password, :password_confirmation

  validates :password,
            presence: true, confirmation: true, length: {within: 6..40}, if: :validate_password?
  validates :password_confirmation,
            presence: true, if: :validate_password?
  validates :email,
            uniqueness: { case_sensitive: false, message: 'This email address is already registered.'},
            format: { with: /.*\@.*\..*/, message: 'is incorrect'}, allow_blank: true

  validates :firstName,
            presence: { message: {message: "First name can't be blank."}},
            format: { with: /\A[a-zA-Z]+\z/,
                      message: {message: "First name must be letters only"}
            }
  validates :lastName,
            presence: { message: { message: "Last name can't be blank."}},
            format: {with: /\A[a-zA-Z]+\z/,
                     message: { message: "Last name must be letters only"}
            }

  before_save       :encrypt_password
  before_validation :downcase_email
  before_destroy    :validate_destroy

  Role::NAMES.each do |name_constant|
    define_method("#{name_constant}?") { self.role.try(:name) == name_constant.to_s }
  end

  def authenticate(password)
    self.encrypted_password == encrypt(password)
  end

  class << self
    def query(params)
      users = User.arel_table
      role = Role.find_by_name('client')
      q = nil

      if params[:count]
        q = users.project('COUNT(*)')
      else
        q = users.project(Arel.star).group(users[:id]).order(users[:email])
      end

      q.where(users[:role_id].eq(role.id))

      q.where(users[:id].eq(params[:id]))                                     if params[:id].present?
      q.where(users[:email].matches("%#{params[:email]}%"))             if params[:email].present?
      q.where(users[:firstName].matches("%#{params[:firstName]}%"))   if params[:firstName].present?
      q.where(users[:lastName].matches("%#{params[:lastName]}%"))     if params[:lastName].present?

      q
    end
  end

  def to_json
    {
      id: id,
      email: email,
      firstName: firstName,
      lastName: lastName
    }
  end

  def validate_destroy
    if self.admin? && User.where(role_id: Role.get_admin.id).count == 1
      self.errors.add :base, 'Can not remove last admin.'
      throw :abort
      false
    end
  end

  private

  def validate_password?
    admin? && (new_record? || !password.nil? || !password_confirmation.nil?)
  end

  def downcase_email
    self.email = self.email.downcase if self.email
  end

  def encrypt_password
    self.salt = make_salt if salt.blank?
    self.encrypted_password = encrypt(self.password) if self.password
  end

  def encrypt(string)
    secure_hash("#{string}--#{self.salt}")
  end

  def make_salt
    secure_hash("#{Time.now.utc}--#{self.password}")
  end

  def secure_hash(string)
    Digest::SHA2.hexdigest(string)
  end

end
