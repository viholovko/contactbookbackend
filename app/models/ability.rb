class Ability
  include CanCan::Ability

  def initialize(user)
      can :manage, :all   if user.admin?

      client_abilities(user)             if !user.admin?
  end

  def client_abilities(user)
    can :index, User
    can :show, User

    # can :create, Contact
    # can :index, Contact

    can [:read], Contact do |contact|
      contact.user_id == user.id
    end

    can :manage, Contact do |contact|
      contact.new_record? || contact.user_id == user.id
    end
  end
end
