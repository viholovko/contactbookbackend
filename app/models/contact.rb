class Contact < ApplicationRecord

  belongs_to :user

  validates :email,
            format: { with: /.*\@.*\..*/, message: 'is incorrect'}, allow_blank: true

  validates :firstName,
            presence: { message: {message: "First name can't be blank."}},
            format: { with: /\A[a-zA-Z]+\z/,
                      message: {message: "First name must be letters only"}
            }
  validates :lastName,
            presence: { message: { message: "Last name can't be blank."}},
            format: {with: /\A[a-zA-Z]+\z/,
                     message: { message: "Last name must be letters only"}
            }
  validates :phone,
            numericality: {message: {message: "Phone number must be numeric"}}

  class << self
    def query(params)
      contacts = Contact.arel_table

      q = nil

      if params[:count]
        q = contacts.project('COUNT(*)')
      else
        q = contacts.project(Arel.star).group(contacts[:id]).order(contacts[:isFavourite].desc)
      end

      q.where(contacts[:id].eq(params[:id]))                                   if params[:id].present?
      q.where(contacts[:user_id].eq(params[:user_id]))                         if params[:user_id].present?
      q.where(contacts[:email].matches("%#{params[:email]}%"))           if params[:email].present?
      q.where(contacts[:firstName].matches("%#{params[:firstName]}%"))   if params[:firstName].present?
      q.where(contacts[:lastName].matches("%#{params[:lastName]}%"))     if params[:lastName].present?
      q.where(contacts[:phone].matches("%#{params[:phone]}%"))           if params[:phone].present?

      q
    end
  end

  def to_json
    {
      id: id,
      email: email,
      firstName: firstName,
      lastName: lastName,
      phone: phone
    }
  end
end
