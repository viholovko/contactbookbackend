json.users @users.each do |user|
  json.extract! user, :id, :email,:firstName, :lastName
  json.created_at user.created_at.strftime("%d-%m-%Y")
end
json.count @count