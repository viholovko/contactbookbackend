json.user do
  json.extract! @user,  :id, :email,:firstName, :lastName
  json.role         @user.role.name
  json.created_at   @user.created_at.strftime("%d-%m-%Y")
end