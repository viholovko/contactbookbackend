json.user do
  json.extract! @user, :id, :email,:firstName, :lastName, :created_at
  # json.avatar paperclip_url user.avatar
  json.role user['role_name']
end