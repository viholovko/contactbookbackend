json.contacts @contacts.each do |contact|
  json.extract! contact, :id, :email,:firstName, :lastName, :phone, :isFavourite, :created_at
  # json.avatar paperclip_url contact.avatar
end
json.count @count