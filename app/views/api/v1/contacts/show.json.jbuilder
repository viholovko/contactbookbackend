json.contact do
  json.extract! @contact, :id, :email,:firstName, :lastName, :phone, :isFavourite, :created_at
  # json.avatar paperclip_url contact.avatar
end