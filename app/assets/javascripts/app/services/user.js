import http from './http';

export function all(filters) {
  let url = '/admin/users.json?';
  Object.keys(filters).forEach(key => url += `${key}=${filters[key]}&`);
  return http.get({url})
}
export function upsert(model = {}){
  let body = new FormData();

  body.append('email', model.email || '' );
  body.append('role', model.role );

  // if (model.avatar && model.avatar.file) body.append('avatar', model.avatar.file);

  if(model.id){
    return http.put({ url:`/admin/users/${model.id}`, body })
  }else{
    return http.post({ url:'/admin/users', body })
  }
}

export function show(id){
  return http.get({url:`/admin/users/${id}.json`})
}

export function destroy(id){
  return http.delete({url:`/admin/users/${id}`})
}