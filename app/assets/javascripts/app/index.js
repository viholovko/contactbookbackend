import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route, IndexRoute } from "react-router";

import BaseLayout from "./components/layouts/Base";
import DashboardLayout from "./components/layouts";
import injectTapEventPlugin from 'react-tap-event-plugin';
import DashboardOverviewPage from "./components/pages/Dashboard/Overview";
import LoginPage from "./components/pages/Login";
import { store, history } from './create_store';
import { check } from './services/sessions';

// generated components paths
import Users from './components/pages/users/index';
import UserForm from './components/pages/users/form';
import User from './components/pages/users/show';


window.onload = function () {
  injectTapEventPlugin();
  ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <Route name="base" path="/" component={BaseLayout}>
        <Route name="dashboard" path='' component={DashboardLayout} onEnter={check}>
          <IndexRoute name="dashboard.overview" component={DashboardOverviewPage} />
          <Route name='users' path='users/:role' component={Users} />
          <Route name='new_user' path='users/:role/new' component={UserForm} />
          <Route name='edit_user' path='users/:role/:id/edit' component={UserForm} />
          <Route name='show_user' path='users/:role/:id' component={User} />
        </Route>
        <Route name="login" path='login' component={LoginPage} />
      </Route>
      <Route path="*" component={LoginPage} />
    </Router>
  </Provider>,
  document.getElementById('content')
  );

};
