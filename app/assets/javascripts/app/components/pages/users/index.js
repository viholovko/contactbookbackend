import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Row, Col, Clearfix } from 'react-bootstrap';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
  FlatButton,
  Dialog,
  IconButton,
  Paper,
  CircularProgress
} from 'material-ui';
import {
  ActionVisibility,
  ImageEdit,
  ActionDelete
} from 'material-ui/svg-icons';
import Select from 'rc-select';
import Pagination from 'rc-pagination';
import en_US from 'rc-pagination/lib/locale/en_US';
import SortingTh from '../../common/sorting_th';
import Filters from '../../common/filters_component';
import { paperStyle } from '../../common/styles';
import { all, destroy } from '../../../services/user';

class Users extends Component {
  state = {
    filters: {
      page: 1,
      per_page: 10,
      show_admin: false
    },
    users: [],
    count: 0,
    showConfirm: false,
    role: ''
  };

  componentWillMount() {
    this.setState({
        role: this.props.params.role
      }, () => this._retrieveUsers()
    );
  };

  componentWillReceiveProps(nextProps) {
    const {role} = nextProps.params;

    if (role !== this.state.role) {
      this.setState({
        role: role
        }, () => this._retrieveUsers()
      );

    }
  }

  _retrieveUsers = () => {
    const { filters } = this.state;
    const {role} = this.props.params;
    all({...filters, role: role}).success(res => {
      this.setState({
        users: res.users,
        count: res.count
      })
    })
  };

  handlePageChange = (page) => {
    this.setState({filters: {...this.state.filters, page}}, this._retrieveUsers);
  };

  handleShowSizeChange = (_,per_page) => {
    this.setState({filters: {...this.state.filters, page: 1, per_page}}, this._retrieveUsers);
  };

  prepareToDestroy = record => {
    this.setState({
      selectedRecord: record,
      showConfirm: true
    })
  };

  updateFilters = (filters = []) => {
    let hash = {};
    filters.forEach(item => Object.keys(item).forEach(key => hash[key] = item[key]));
    this.setState({
      filters: {
        ...this.state.filters,
        ...hash,
        page: 1
      }
    }, this._retrieveUsers)
  };

  closeConfirm = () => {
    this.setState({ showConfirm: false })
  };

  handleDelete = () => {
    const { selectedRecord } = this.state;
    destroy(selectedRecord.id).success(res => {
      this._retrieveUsers();
      this.closeConfirm();
    });
  };

  render() {
    const { isLoading } = this.props.app.main;
    const {role} = this.props.params;
    const { users, showConfirm, count } = this.state;
    const { page, per_page } = this.state.filters;
    const { palette } = this.context.muiTheme;

    return (
      <Paper style={paperStyle} zDepth={1}>
        <h2>{ I18n.t(`${role}.header`) }</h2>
        <Row>
          <Col sm={8}>
            <Pagination
              selectComponentClass={Select}
              onChange={this.handlePageChange}
              showQuickJumper={true}
              showSizeChanger={true}
              pageSizeOptions={['10','20','50']}
              pageSize={per_page}
              onShowSizeChange={this.handleShowSizeChange}
              current={page}
              total={count}
              locale={en_US}
            />
          </Col>
          <Col sm={4} className="text-right" style={{minHeight:61}}>
            <CircularProgress className={isLoading ? 'loading-spinner' : 'hidden'} size={36} />
          </Col>
        </Row>
        <Filters columns={[
          { label: I18n.t('client.fields.firstName'),    key: 'firstName',     type: 'string'  },
          { label: I18n.t('client.fields.lastName'),     key: 'lastName',      type: 'string'  },
          { label: I18n.t('client.fields.email'),    key: 'email',     type: 'string'  }
        ]} update={this.updateFilters} />
        <Table style={{ tableLayout: 'auto' }} fixedHeader={false}>
          <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
            <TableRow>
              <TableHeaderColumn>
                <SortingTh update={this.updateFilters} column='firstName'>
                  { I18n.t('client.fields.firstName') }
                </SortingTh>
              </TableHeaderColumn>
              <TableHeaderColumn>
                <SortingTh update={this.updateFilters} column='lastName'>
                  { I18n.t('client.fields.lastName') }
                </SortingTh>
              </TableHeaderColumn>
              <TableHeaderColumn>
                <SortingTh update={this.updateFilters} column='email'>
                  { I18n.t('client.fields.email') }
                </SortingTh>
              </TableHeaderColumn>
              <TableHeaderColumn>
                <SortingTh update={this.updateFilters} column='created_at'>
                  { I18n.t('client.fields.created_at') }
                </SortingTh>
              </TableHeaderColumn>
              <TableHeaderColumn>

              </TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false}>
            {
              users.map(item => {
                return (
                  <TableRow key={item.id}>
                    <TableRowColumn>{ item.firstName }</TableRowColumn>
                    <TableRowColumn>{ item.lastName }</TableRowColumn>
                    <TableRowColumn>{ item.email  }</TableRowColumn>
                    <TableRowColumn>{ item.created_at  }</TableRowColumn>
                    <TableRowColumn className='text-right'>
                      <IconButton onTouchTap={() => location.hash = `#/users/${role}/${item.id}`}>
                        <ActionVisibility color={palette.primary1Color}/>
                      </IconButton>
                      <IconButton onTouchTap={this.prepareToDestroy.bind(this,item)}>
                        <ActionDelete color="#c62828" />
                      </IconButton>
                    </TableRowColumn>
                  </TableRow>
                )
              })
            }
          </TableBody>
        </Table>
        <Dialog
          title={ I18n.t('headers.are_you_sure') }
          actions={[
            <FlatButton
              onTouchTap={this.closeConfirm}
              label={ I18n.t('actions.cancel') }
            />,
            <FlatButton
              secondary={true}
              onTouchTap={this.handleDelete}
              label={ I18n.t('actions.confirm') }
            />
          ]}
          modal={false}
          open={showConfirm}
          onRequestClose={this.closeConfirm}
        >
          { I18n.t('you_are_going_to_remove') } user.
        </Dialog>
        <Clearfix/>
      </Paper>
    )
  }
}

Users.contextTypes = {
  muiTheme: PropTypes.object.isRequired
};

export default connect(state => state)(Users)