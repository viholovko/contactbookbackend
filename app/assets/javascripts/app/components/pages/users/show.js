import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  Row,
  Col,
  ControlLabel,
  Clearfix
} from 'react-bootstrap';
import {
  Paper,
  RaisedButton,
} from 'material-ui';
import {paperStyle} from '../../common/styles';
import {show} from '../../../services/user';
import PropTypes from "prop-types";

class User extends Component {
  state = {
    user: {
      email: ''
    },
    role: '',
    id: 0,
    validationErrors: {},
  };

  componentWillMount() {
    this._retrieveUser();
  };

  componentWillReceiveProps(nextProps) {
    const {role, id} = nextProps.params;
    if (this.state.role === '' || this.state.id === 0) {
      this.setState({
        ...this.state,
        role: role,
        id: id
      })
    }else {
      if (role !== this.state.role || id !== this.state.id) {
        this.setState({
          ...this.state,
          role: role,
          id: id
          }, () => this._retrieveUser()
        );
      }
    }
  }

  _retrieveUser = () => {
    const {id} = this.props.params;
    if (!id) {
      return false
    }
    show(id).success(res => {
      this.setState({
        user: res.user
      })
    })
  };

  handleChange = (key, value) => {
    const {user} = this.state;

    this.setState({
      user: {
        ...user,
        [key]: value
      },
      validationErrors: {
        ...this.state.validationErrors,
        [key]: null
      }
    })
  };

  render() {
    const {user} = this.state;
    const { palette } = this.context.muiTheme;
    return (
      <Paper style={paperStyle} zDepth={1}>
        <Row>
          <Col sm={4}>
            <ControlLabel><h2>{user.email}</h2></ControlLabel>
          </Col>
          <Col sm={8}>
            <RaisedButton href={`#/users/${user.role}`} className='pull-right' secondary={true} label='Back'/>
          </Col>
        </Row>
        <hr/>
        <Row>
          <Col lg={6} md={6} sm={6} xs={6}>
            <ControlLabel>{I18n.t('client.fields.firstName')}</ControlLabel>
            <br/>
            <span className="form-control-static"> {user.firstName || '-'}</span>
          </Col>
          <Col lg={6} md={6} sm={6} xs={6}>
            <ControlLabel>{I18n.t('client.fields.lastName')}</ControlLabel>
            <br/>
            <span className="form-control-static"> {user.lastName || '-'}</span>
          </Col>
        </Row>
        <Row>
          <Col lg={6} md={6} sm={6} xs={6}>
            <ControlLabel>{I18n.t('client.fields.email')}</ControlLabel>
            <br/>
            <span className="form-control-static"> {user.email || '-'}</span>
          </Col>
          <Col lg={6} md={6} sm={6} xs={6}>
            <ControlLabel>{I18n.t('client.fields.created_at')}</ControlLabel>
            <br/>
            <span className="form-control-static"> {user.created_at || '-'}</span>
          </Col>
        </Row>
        <hr/>
        <Clearfix/>
      </Paper>
    )
  }
}

User.contextTypes = {
  muiTheme: PropTypes.object.isRequired
};

export default connect(state => state)(User)