import React, { Component } from 'react';
import { Paper, Chip, Card, CardActions, CardHeader,
         CardMedia, CardTitle, CardText, FlatButton, RadioButton, RadioButtonGroup,
         DatePicker, RaisedButton, Toggle, SelectField, MenuItem
} from 'material-ui';
import { paperStyle } from '../../common/styles';
import { BarChart, Bar, XAxis, YAxis, Tooltip, LineChart, Line, CartesianGrid, Legend} from 'recharts';
import { all } from '../../../services/piechart_data';
import { Row, Col, ControlLabel } from 'react-bootstrap';

class Blank extends Component {
  state = {
    autoOk: false,
    value: 1,
    data: [],
    style: {
      chip: {
        margin: 4,
      },
      wrapper: {
        display: 'flex',
        flexWrap: 'wrap',
      },
      charts: {
        margin: 10
      }
    }
  };

  handleChangeFromDate = (event, date) => {
    this.setState({
      from_date: date,
    });
  };

  handleChangeToDate = (event, date) => {
    this.setState({
      to_date: date,
    });
  };

  handleChange = (event, index, value) => this.setState({
    value: value
  });

  componentWillMount() {
    this._retrieveData();
  };

  _retrieveData = () => {
    all().success(res => {
      this.setState({
        data: res.data,
      })
    })

  };
  updateFilters = () => {
    all().success(res => {
      this.setState({
        data: res.data,
      })
    })
  };

  PieCharts = (dataType,labelText) =>{
    const {style} = this.state;
    return(
      <div>
        <Col xs={20} style={style.charts}>
          <Row >
            <Col xs={10}>
              <Row>
                <BarChart
                  width={400}
                  height={200}
                  data={dataType}
                  margin={{ top: 5, right: 20, left: 10, bottom: 5 }}
                  barGap={2}
                >
                  <XAxis dataKey="name" />
                  <YAxis allowDecimals={false} padding={{top: 10}} />
                  <Bar dataKey='count' barSize={30} fill='#8884d8'/>
                  <Tooltip/>
                </BarChart>
              </Row>
              <Row>
                <ControlLabel>
                  {labelText}
                </ControlLabel>
              </Row>
              <Row>
                <div style={style.wrapper}>
                  { dataType.map((item,index) => <Chip key={index} labelColor="white" backgroundColor={dataType[index].color} style={style.chip}>
                    { dataType[index].name + " " + dataType[index].count}
                  </Chip>)}
                </div>
              </Row>
            </Col>
          </Row>
        </Col>
      </div>
    )
  };

  render() {
    const { style, data} = this.state;

    const userData = [{name: "Admin",  count: data.admin, color: "#ff6262"},
      {name: "Client", count: data.client,  color: "#FF8042"}
    ];

    return (
      <Paper style={paperStyle} zDepth={1}>
        <Card>
          <CardText>
            <div style={style.wrapper}>
              { this.PieCharts(userData,"Relationship User's Role: ")}
            </div>
          </CardText>
        </Card>
      </Paper>
    );
  }
}

export default Blank;