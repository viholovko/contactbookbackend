import React, { Component } from 'react';
import { Col } from 'react-bootstrap';
import DropZone from 'react-dropzone';
import http from '../../services/http';

const styles = {
  container: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    flexWrap: 'wrap'
  },
  item: {
    flex: '1 0 15%',
    height: '120px',
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    borderRadius: '4px',
    position: 'relative',
    marginRight: '6px',
    marginBottom: '6px'
  },
  removeIcon: {
    position: 'absolute',
    top: '8px',
    right: '10px',
    color: 'gray',
    fontSize: '20px',
    cursor: 'pointer'
  }
};

export default class ImagesComponent extends Component {
  state = {
    files: []
  };

  componentWillReceiveProps(nextProps) {
    const { value } = nextProps;
    if (value instanceof Array) {
      this.setState({
        files: value
      })
    }
  }

  updateParent() {
    const { update } = this.props;
    const { files } = this.state;
    update(files, 'photo');
  }

  handleDrop = files => {
    this.setState({
      files: [...this.state.files, ...files]
    }, this.updateParent)
  };

  handleRemove = file => {
    const { files } = this.state;
    let index = files.indexOf(file);
    if (file.id) {
      file['_destroy'] = true;
      this.setState({
        files: files
      }, this.updateParent)
    }else {
      this.setState({
        files: files.filter(item => item != file)
      }, this.updateParent)
    }

  };

  render() {
    const { files } = this.state;

    return(
      <div
        style={styles.container}
      >
        {
          files.map((image, index) => {
            return (
              image['_destroy']
                ?
                null
                :
                <div
                  key={index}
                  style={{...styles.item, backgroundImage: `url(${ image.url || image.preview })`}}
                >
                <span
                  style={styles.removeIcon}
                  onClick={ () => this.handleRemove(image) }
                >
                  &times;
                </span>
                </div>
            )
          })
        }
        <Col xs={6} md={3}>
          <div
            style={{...styles.item, border: "2px dashed #ccc", cursor: 'pointer'}}
          >
            <DropZone
              onDrop={this.handleDrop}
              style={{width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center'}}
            >Add image</DropZone>
          </div>
        </Col>
      </div>
    )
  }
}
