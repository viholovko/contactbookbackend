import React, { Component } from 'react';
import { Col, Row } from 'react-bootstrap';
import DropZone from 'react-dropzone';
import http from '../../services/http';

const styles = {
  container: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    flexWrap: 'wrap'
  },
  item: {
    height: '300px',
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    borderRadius: '4px',
    position: 'relative',
    // marginRight: '6px',
    marginBottom: '6px'
  },
  removeIcon: {
    position: 'absolute',
    right: '25px',
    color: 'gray',
    fontSize: '20px',
    cursor: 'pointer'
  }
};

export default class VideosComponent extends Component {
  state = {
    files: []
  };

  componentWillReceiveProps(nextProps) {
    const { value } = nextProps;
    if (value instanceof Array) {
      this.setState({
        files: value
      })
    }
  }

  updateParent() {
    const { update } = this.props;
    const { files } = this.state;
    update(files, 'video');
  }

  handleDrop = files => {
    this.setState({
      files: [...this.state.files, ...files]
    }, this.updateParent)
  };

  handleRemove = file => {
    const { files } = this.state;
    let index = files.indexOf(file);
    if (file.id) {
      file['_destroy'] = true;
      this.setState({
        files: files
      }, this.updateParent)
    }else {
      this.setState({
        files: files.filter(item => item != file)
      }, this.updateParent)
    }

  };

  render() {
    const { files } = this.state;

    return(
      <Row
        style={styles.container}
      >
        {
          files.map((video, index) => {
            return (
              video['_destroy']
                ?
                null
                :
                <Col md={6} key={index}
                     style={{...styles.item}}
                     className='video-preview'
                >
                  {
                    <video
                      src={`${ video.url || video.preview}`}
                      controls={true}
                      style={{...styles.item, width: '100%', margin: 0, backgroundColor: 'lightgray'}}
                    />
                  }
                  <span
                    style={styles.removeIcon}
                    onClick={ () => this.handleRemove(video) }
                  >
                    &times;
                  </span>
                </Col>
            )
          })
        }
        <Col md={6}>
          <div
            style={{...styles.item, border: "2px dashed #ccc", cursor: 'pointer'}}
          >
            <DropZone
              accept='video/mp4'
              onDrop={this.handleDrop}
              style={{width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center'}}
            >Add video</DropZone>
          </div>
        </Col>
      </Row>
    )
  }
}
