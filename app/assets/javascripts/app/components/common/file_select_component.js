import React, { Component } from 'react';
import {FloatingActionButton, TextField} from 'material-ui';
import ContentAdd from 'material-ui/svg-icons/content/add';
import http from "../../services/http";

//Exemple Format
//audio/*
//image/*
//video/*
//image/*,video/*
//image/jpeg,image/png

export default class FileSelect extends Component {
  constructor(props){
    super(props);

    this.state = {
      secondary: false
    };

  }

  handleSelect=(files)=>{
    let { onChange, errorUpdate, backend, entity_type } = this.props;

    this.onRemove();
    errorUpdate(this._errorsClear(files))


    if(backend){
      let body = new FormData();

      files.forEach((e,i,a)=> e.file && body.append(`files[]`, e.file))

      http.promise(http.post({ url:`/admin/attachments/${entity_type}`, body })).then(
          (res)=>{
            onChange(files.map((e,i,a)=>({...e, ...res[i]})));
          }, (res)=> {onChange(files);errorUpdate(res)})
    }else{
      onChange(files);
    }

    this.setState({secondary: true})
  }

  onRemove=()=>{
    let {files, backend} = this.props

    files.forEach((e,i,a)=>{
      if(e.id && backend){
        http.promise(http.delete({ url:`/admin/attachments/${e.id}.json` }))
      }
    })
  }


  attachmentErrorName(i){
    return `attachment[${i}]`;
  }

  _errorsClear=(files)=>{
    let { errorName } = this.props;
    let errors = {}
    let errorSourseName = errorName || this.attachmentErrorName;

    for(let i=0;i<files.length;i++){
      errors[errorSourseName(i)]=null;
    }

    return errors;
  }

  render() {
    let {secondary} = this.state;
    let {disabled, multiple, backend, accept, errors, errorName, files} = this.props;

    let errorSourse = errors;
    let errorSourseName = errorName || this.attachmentErrorName;

    let error = ''

    if(backend || multiple){
      error = files.map((e,i,a)=>errorSourse[`${errorSourseName(i)}`]).filter((e)=>e!=undefined&&e!=null).join('; \n');
    }else{
      error = errorSourse
    }

    let names = files.map((e,i,a)=>(e.file && e.file.name)||e.name)

    names = names.join(';\n') || I18n.t('headers.file_not_select')

    return (
        <div style={{display: 'flex'}}>
          <FloatingActionButton secondary={secondary} disabled={disabled} onTouchTap={(e) => this.fileUploader.click()}>
            <ContentAdd />
            <input type="file" {... multiple ? {multiple:true} : {}} accept={accept} ref={(fileUploader)=> this.fileUploader=fileUploader} onChange={(val)=> this.handleSelect(Array.from(val.target.files).map((e,i,a)=>({file:e})))}/>
          </FloatingActionButton>
          <TextField
              disabled={true}
              value={names}
              errorText={error}
              style={{marginLeft: 24}}
              multiLine={multiple}
          />
        </div>
    )
  }

}