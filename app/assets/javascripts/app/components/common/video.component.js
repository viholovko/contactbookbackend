import React, { Component } from 'react';
import { Col } from 'react-bootstrap';
import DropZone from 'react-dropzone';
import http from '../../services/http';

const styles = {
  container: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    flexWrap: 'wrap'
  },
  item: {
    flex: '1 0 15%',
    height: '250px',
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    borderRadius: '4px',
    position: 'relative',
    marginRight: '6px',
    marginBottom: '6px'
  },
  removeIcon: {
    position: 'absolute',
    top: '8px',
    right: '10px',
    color: 'gray',
    fontSize: '20px',
    cursor: 'pointer'
  }
}
export default class Video extends Component {

  state = {
    page: 1,
    per_page: 10,
    loadedCount: 0
  };

  _handleDrop = files => {
    let { onChange, value = [] } = this.props;
    let new_video = {file: files[0], url: files[0].preview};
    onChange(new_video)
  };

  _handleRemove = () => {
    const { value, onChange } = this.props;

    if (value && value.id) {
      value['_destroy'] = true
    }

    onChange('')
  };

  render() {
    const { value = [] } = this.props;
    const { loadedCount } = this.state;
    return(
      <div
        style={styles.container}
      >
        {
          value && value.url
          ?
          <div
            style={{...styles.item}}
          >
            <video
              src={`${ value.url }#t=10,20`}
              controls
              style={{...styles.item, width: '100%', height: '100%', margin: 0, backgroundColor: 'lightgray'}}
            />
            <span
              style={styles.removeIcon}
              onClick={ () => this._handleRemove() }
            >
              &times;
            </span>
          </div>
          :
          <div
          style={{...styles.item, border: "2px dashed #ccc", cursor: 'pointer'}}
          >
            <DropZone
              onDrop={this._handleDrop}
              style={{width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center'}}
            >Add video</DropZone>
          </div>
        }
      </div>
    )
  }
}