import React, { Component } from 'react';
import { Col, Row } from 'react-bootstrap';
import DropZone from 'react-dropzone';
import http from '../../services/http';

export default class MediaComponent extends Component {
  state = {
    files: []
  };

  componentWillReceiveProps(nextProps) {
    const { value } = nextProps;
    if (value instanceof Array) {
      this.setState({
        files: value
      })
    }
  }

  _parseFiles = (type, file) => {
    switch (type) {
      case 'photo':
        return(
          <div
            className='image-preview'
            style={{backgroundImage: `url(${file.url})`,
              height:360, width:480, borderRadius:8}}
          >
          </div>
        );
      case 'video':
        return(
          <div
            className='video-preview'
            style={{height: 'auto'}}
          >
            {
              <video
                src={`${ file.url }`}
                controls={true}
                width={480}
                height={360}
                style={{marginTop: '10px', marginBottom: '10px'}}
              />
            }
          </div>
        );
    }
  };

  render() {
    const { files } = this.state;

    return(
      <Row>
        <br/>
        {
          files.map((file,i) => {
            return (
                <Col sm={6} key={i}>
                  { this._parseFiles(file.type, file) }
                </Col>
            )
          })
        }
        <br/>
      </Row>
    )
  }
}
