import React, { Component } from "react";
import { Grid, Row, Col } from 'react-bootstrap';
import {AppBar, Drawer, MenuItem, IconButton, Popover, Menu} from 'material-ui';
import { logout } from '../../services/sessions';
import { LogoutIcon, DashboardIcon, PostIcon, AdsIcon, ClassroomIcon } from '../common/icons';
import { SocialGroup, ContentContentPaste, NavigationArrowDropRight, ActionSettings } from 'material-ui/svg-icons'
import { store } from '../../create_store';
const { dispatch } = store;
import { connect } from 'react-redux';

class HomePage extends Component {
  state = { open: false };

  handleToggle = () => this.setState({open: !this.state.open});

  _selectLanguage = (language) => {
    this.setState({
      languagePopoverOpen: false
    })
    dispatch({type: 'SET_LANGUAGE', language});
  }

  render() {
    return (
        <div className="dashboard-page">
          <AppBar
              title="Contact Book"
              onLeftIconButtonTouchTap={ this.handleToggle }
          >
            <IconButton
                style={{marginTop: '10px'}}
                onTouchTap={(event => this.setState({languagePopoverOpen: true, anchorEl: event.currentTarget}))}
            >
              <div className={`flag ${ this.props.app.main.language }`}></div>
            </IconButton>
            <Popover
                open={this.state.languagePopoverOpen}
                anchorEl={this.state.anchorEl}
                anchorOrigin={{horizontal: 'right', vertical: 'bottom'}}
                targetOrigin={{horizontal: 'right', vertical: 'top'}}
                onRequestClose={() => this.setState({languagePopoverOpen: false})}
            >
              <Menu>
                <MenuItem onTouchTap={() => this._selectLanguage('ua')}>
                  <div className="flag ua"></div>
                  <span style={{marginLeft: '10px'}}>UKR</span>
                </MenuItem>
                <MenuItem onTouchTap={() => this._selectLanguage('en')}>
                  <div className="flag en"></div>
                  <span style={{marginLeft: '10px'}}>ENG</span>
                </MenuItem>
              </Menu>
            </Popover>

            <IconButton
              style={{marginTop: '10px'}}
              onTouchTap={logout}
            >
              <LogoutIcon/>
            </IconButton>
          </AppBar>
          <Drawer
            docked={false}
            width={300}
            open={this.state.open}
            onRequestChange={(open) => this.setState({open})}
          >
            <MenuItem onTouchTap={this.handleToggle} href='#/' leftIcon={<DashboardIcon />}>
              { I18n.t('headers.dashboard') }
            </MenuItem>

            <MenuItem onTouchTap={this.handleToggle} href='#/users/client' leftIcon={<SocialGroup/>} >
              { I18n.t('headers.users') }
            </MenuItem>
          </Drawer>
          <Grid>
            <Row>
              <Col md={12}>
                <br/>
                {this.props.children}
              </Col>
            </Row>
          </Grid>
        </div>
    );
  }

}

export default connect(state => state)(HomePage);
