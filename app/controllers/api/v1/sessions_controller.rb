class Api::V1::SessionsController < Api::V1::BaseController

  skip_before_action :authenticate_user, except: [:destroy]

  def create
    @user = User.find_by_email params[:email]

    if @user&.authenticate params[:password]
      sign_in user: @user, push_token: params[:push_token]

      render json: {session_token: current_session.token, user: current_user.to_json}
    else
      render json: {errors: [{message: "User not found."}]}, status: :bad_request
    end
  end

  def destroy
    sign_out
    render json: {message: 'Logout successful'}
  end

  def update
    @session = current_session
    if @session.update_attributes update_params
      render json: {message: 'Update successful'}
    else
      render json: {errors: @current_session.errors.map{|k, v| v}}, status: :bad_request
    end
  end

  private

  def logout_params
    params.permit(:session_token)
  end

  private

  def update_params
    allowed_params = params.permit :push_token, :token
    allowed_params
  end
end