class Api::V1::ContactsController < Api::V1::BaseController

  load_and_authorize_resource :contact

  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1

    query = Contact.query search_params
    count_query = Contact.query search_params.merge count: true

    @contacts = Contact.find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)
    @count = Contact.find_by_sql(count_query.to_sql).first['count']
  end

  def create
    if @contact.save
      render json: {
        contact: @contact.to_json
      }
    else
      render json: {errors: @contact.errors.map{|k, v| v}}, status: :bad_request
    end
  end

  def update
    if @contact.update_attributes contact_params
      render json: {
        contact: @contact.to_json
      }
    else
      render json: { errors: @contact.errors.map{|k, v| v} }, status: :bad_request
    end
  end

  def show
  end

  def destroy
    @contact.destroy
    render json: {message: 'Contact is deleted'}
  end

  private

  def contact_params
    allowed_params = params.permit :firstName, :lastName, :email, :phone, :avatar
    allowed_params[:user_id] = current_user.id if @contact.nil?

    allowed_params
  end

  def search_params
   allowed_params = params.permit :sort_column, :sort_type, :name
   allowed_params[:user_id] = current_user.id

   allowed_params
  end

end