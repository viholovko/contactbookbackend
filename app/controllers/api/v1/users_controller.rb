class Api::V1::UsersController < Api::V1::BaseController

  load_and_authorize_resource :user, only: :show
  skip_before_action :authenticate_user, only: [:create]

  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1

    query = User.query search_params
    count_query = User.query search_params.merge count: true

    @users = User.find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)
    @count = User.find_by_sql(count_query.to_sql).first['count']
  end

  def create
    @user = User.new create_params

    @user.role = Role.get_client

    if @user.save
      render json: {
          user: @user.to_json
      }
    else
      render json: {errors: @user.errors.map{|k, v| v}}, status: :bad_request
    end
  end

  def show
    @user = User.find params[:id]
    authorize! :show, @user
  end

  def profile
    @user = current_user
    render 'show'
  end

  private

  def create_params
    params.permit :email, :firstName, :lastName, :password
  end

  def search_params
   allowed_params = params.permit :sort_column, :sort_type, :name
   allowed_params[:current_user] = current_user

   allowed_params
  end

end