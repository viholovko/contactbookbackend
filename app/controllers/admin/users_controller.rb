class Admin::UsersController < Admin::BaseController

  load_and_authorize_resource :user

  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1

    query = User.query params.merge(current_user: current_user)
    count_query = User.query params.merge(count: true, current_user: current_user)

    @users = User.find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)
    @count = User.find_by_sql(count_query.to_sql).first['count']
  end

  def create
    @user = User.new user_params

    if @user.save
      render json: { message: I18n.t('client.messages.success_upsert') }
    else
      render json: { validation_errors: @user.errors }, status: :unprocessable_entity
    end
  end

  def update
    if @user.update_attributes user_params
      render json: { message: I18n.t('client.messages.success_upsert') }
    else
      render json: { validation_errors: @user.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    if @user.destroy
      render json: {message: I18n.t('client.messages.success_destroy')}
    else
      render json: {errors: @user.errors.full_messages }, status: :unprocessable_entity
    end
  end

  def show
    # @user = User.find_by_sql(query.to_sql).first
  end

  private

  def user_params
    allowed_params = params.permit :email, :password, :password_confirmation, :firstName, :lastName, :avatar, :phone_number, :country_id
    allowed_params[:role_id] = Role.send("get_#{ params[:role] }").id
    allowed_params
  end

  def classroom_params
    allowed_params = params.permit
    allowed_params[:current_user] = @user
    allowed_params[:accepted] = true
    allowed_params


  end

end