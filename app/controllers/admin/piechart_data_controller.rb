class Admin::PiechartDataController < Admin::BaseController
  def index
      count_user_type = User.find_by_sql("SELECT count(case when roles.name = 'admin' then 1 else null end) as admins,
                                       count(case when roles.name = 'client' then 1 else null end) as clients
                               FROM users JOIN roles ON users.role_id = roles.id")
      @data = {}
      @data[:admin] =        count_user_type[0]['admins']
      @data[:client] =      count_user_type[0]['client']
  end
end
