class AddClientContacts < ActiveRecord::Migration[5.1]
  def change
    create_table :contacts do |t|
      t.string :firstName
      t.string :lastName
      t.string :email
      t.string :phone
      t.boolean :isFavourite, default: false
      t.integer :user_id
      t.timestamps
    end

    add_attachment :contacts, :avatar
  end
end
