class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :encrypted_password
      t.string :salt
      t.string :email
      t.string :firstName
      t.string :lastName
      t.references :role
      t.datetime :last_logged_in

      t.timestamps
    end
    add_attachment :users, :avatar
  end
end