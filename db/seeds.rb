# Dir[File.join(Rails.root, 'db', 'seeds', '*.rb')].sort.each { |seed| load seed }

puts 'Create admin ...'
User.create email: 'admin@gmail.com',
            password: 'secret',
            password_confirmation: 'secret',
            firstName: 'Admin',
            lastName: 'Admin',
            role_id: Role.get_admin.id