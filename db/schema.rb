# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180115114412) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "ads", force: :cascade do |t|
    t.string "title"
    t.text "body"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "attachments", id: :serial, force: :cascade do |t|
    t.integer "attachable_id"
    t.string "attachable_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "file_file_name"
    t.string "file_content_type"
    t.integer "file_file_size"
    t.datetime "file_updated_at"
    t.index ["attachable_id"], name: "index_attachments_on_attachable_id"
  end

  create_table "chats", id: :serial, force: :cascade do |t|
    t.string "title"
    t.integer "chat_type"
    t.integer "owner_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "avatar_file_name"
    t.string "avatar_content_type"
    t.integer "avatar_file_size"
    t.datetime "avatar_updated_at"
  end

  create_table "chats_users", id: false, force: :cascade do |t|
    t.integer "chat_id"
    t.integer "user_id"
    t.index ["chat_id"], name: "index_chats_users_on_chat_id"
    t.index ["user_id"], name: "index_chats_users_on_user_id"
  end

  create_table "classrooms", force: :cascade do |t|
    t.string "name"
    t.integer "assessment_file_id"
    t.string "assessment_link"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["assessment_file_id"], name: "index_classrooms_on_assessment_file_id"
  end

  create_table "code_requests", force: :cascade do |t|
    t.string "code"
    t.bigint "country_id"
    t.string "phone_number"
    t.integer "attempts_count"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["country_id"], name: "index_code_requests_on_country_id"
  end

  create_table "countries", force: :cascade do |t|
    t.string "name"
    t.string "phone_code"
    t.string "alpha2_code"
    t.string "alpha3_code"
    t.string "numeric_code"
    t.string "flag_file_name"
    t.string "flag_content_type"
    t.integer "flag_file_size"
    t.datetime "flag_updated_at"
    t.boolean "selected"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "email_senders", id: :serial, force: :cascade do |t|
    t.string "address"
    t.string "port"
    t.string "domain"
    t.string "authentication"
    t.string "user_name"
    t.string "password"
    t.boolean "enable_starttls_auto"
  end

  create_table "friendships", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "friend_id"
    t.integer "status"
    t.datetime "accepted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "post_feedbacks", force: :cascade do |t|
    t.bigint "post_id"
    t.string "type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["post_id"], name: "index_post_feedbacks_on_post_id"
  end

  create_table "posts", force: :cascade do |t|
    t.bigint "user_id"
    t.text "text"
    t.string "type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["user_id"], name: "index_posts_on_user_id"
  end

  create_table "privacy_policies", force: :cascade do |t|
    t.text "body"
  end

  create_table "roles", id: :serial, force: :cascade do |t|
    t.string "name"
  end

  create_table "sessions", id: :serial, force: :cascade do |t|
    t.string "token"
    t.integer "user_id"
    t.string "push_token"
    t.integer "device_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_sessions_on_user_id"
  end

  create_table "terms", force: :cascade do |t|
    t.text "body"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "encrypted_password"
    t.string "salt"
    t.string "email"
    t.string "firstName"
    t.string "lastName"
    t.string "phone_number"
    t.integer "country_id"
    t.integer "role_id"
    t.datetime "last_logged_in"
    t.text "about_me"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "avatar_file_name"
    t.string "avatar_content_type"
    t.integer "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.decimal "latitude", precision: 10, scale: 6
    t.decimal "longitude", precision: 10, scale: 6
    t.index ["country_id"], name: "index_users_on_country_id"
    t.index ["role_id"], name: "index_users_on_role_id"
  end

  add_foreign_key "post_feedbacks", "posts"
  add_foreign_key "posts", "users"
end
